# aplicando regionalizacao arbitraria
## banco iniciou o processo com 111 variaveis
library(readxl)
library(plyr)
library(dplyr)
head(dados)

### criando uf e regiao para hospedar alteracoes arbitrarias
dados$UF6<-as.character(dados$UF4)
dados$Região6<-as.character(dados$Região3)

### criando codigo unico (ano+acao+localizador) para identificar alteracoes na base geral
dados$cod.uni<-paste0(dados$Ano,substr(dados$`Ação (Cod/Desc)`,1,4),substr(dados$`Subtítulo (Cod/Desc)`,1,4))


 
#############_____________________ ABRINDO E MANIPULANDO BASE DE ALTERACOES (USADO PELO NELSON)  
# Arbitrario
base.alter<-read.csv2("11. Regionalização SIGA Final\\data\\Arbitrario.csv")
base.alter$cod.uni<-paste0(base.alter$Ano,substr(base.alter$Acao,1,4),substr(base.alter$Subtitulo,1,4))

levels(base.alter$Regiao3)<-c("CO","ND","NE","NO","SD","SD","SL")

x<-table(base.alter$UF5a)
x2<-table(base.alter$UF4)

cbind(x2,x)


####### METODO A - USANDO A CHAVE PRIMARIA
# 
# base.alter$teste<-base.alter$UF4==base.alter$UF5a
# table(base.alter$teste)
# base.altera<-base.alter[base.alter$teste==F,]
# base.alter2<-base.altera[,c(1,5,7,60)]
# base.alter3<-unique(base.alter2)
# 
# names(base.alter3)<-c("cod","Regiao3a","UF5a","cod.uni")
# 
# base.alter3$Regiao3a<-as.character(base.alter3$Regiao3a)
# dadosA<-left_join(dados,base.alter3, by="cod")




####### METODO B - USANDO A COMBINACAO (ANO+ACAO+SUBTITULO)
base.alter$teste<-base.alter$UF4==base.alter$UF5a
table(base.alter$teste)
base.altera<-base.alter[base.alter$teste==F,]
base.alter2<-base.altera[,c(5,7,60)]
base.alter3<-unique(base.alter2)

# TESTES
# z<-table(base.alter3$cod.uni)
# z2<-z[z>=2]
# 
# base.teste<-base.alter[,c(60)]
# base.teste3<-unique(base.teste)
# 
# write.csv2(base.alter,"base_alterada.csv")
# write.csv2(z2,"repeticoes_cod.csv")

names(base.alter3)<-c("Regiao3a","UF5a","cod.uni")
base.alter3$Regiao3a<-as.character(base.alter3$Regiao3a)

dados2<-left_join(dados,base.alter3, by="cod.uni")


# dados2$UF6[dados2$cod==c(1226048,1239828,1260236,1289803,1291992,2433055,2442483,2447067,2449993,2467799,2481722,2508267,2552218,2563360)]<-
# <-c(2342505,2346117,2356701,2357538,2358786,2361160)

dados2$UF6[dados2$UF!="ND" & dados2$UF!=dados2$UF6]<-dados2$UF[dados2$UF!="ND" & dados2$UF!=dados2$UF6]


#########################################################____________________________________ TESTES E ALTERACOES FINAIS
#dados2<-dadosA

y<-table(dados2$UF5a)
sum(y)


dados2$UF6[!is.na(dados2$UF5a)]<-as.character(dados2$UF5a[!is.na(dados2$UF5a)])
dados$Região6[!is.na(dados2$Regiao3a)]<-as.character(dados2$Regiao3a[!is.na(dados2$Regiao3a)])

table(dados2$UF6)
table(dados2$UF4)
table(dados2$Região6)
dados2$UF6[dados2$UF6=="ND"&dados2$Região6!="ND"]<-dados2$Região6[dados2$UF6=="ND"&dados2$Região6!="ND"]
table(dados2$UF6)

###################### DEFININDO OS DADOS ALTERADOS ARBITRARIAMENTE
dados2$alter.arbitr<-!is.na(dados2$UF5a)
dados.alterados<-dados2[dados2$alter.arbitr==T,]

sum(dados.alterados$`Despesa Executada`)
sum(dados.alterados$Empenhado)

write.csv2(dados.alterados,"DADOS_ALTERADOS.csv")


max(dados2$cod)


