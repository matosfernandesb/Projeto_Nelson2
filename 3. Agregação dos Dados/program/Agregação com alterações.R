########################## LENDO DADOS ORIGINAIS PARA OBTER OS CODIGOS CORRETOS NO SUBTITULO

######### ABRINDO AS BASES DE DADOS
# Loc: tecwin2 > Matheus > investimento_federal > invest_fed
dados_SIGA<-read.table(file.choose(),h=T,stringsAsFactors = F)

SIGA_dados<-dados_SIGA
### OBTENDO OS CODIGOS DO ORGAO VIA SIOP
load(file.choose())
dados_SIOP<-SIOP_dados

######### AJUSTANDO AS VARIAVEIS

### OBTENDO OS SUBTITULOS CORRETOS
dados_SIGA$Subtitulo2<-substr(as.character(dados_SIGA$Funcional),18,21)

### FILTRO DE ANO
dados_SIGA2<-subset(dados_SIGA, dados_SIGA$Ano>2000)
dados_SIOP2<-subset(dados_SIOP, dados_SIOP$`Ano Exerc�cio`<2015)

### AJUSTE NAS UF DO SIGA
dados_SIGA2$UF2<-dados_SIGA2$UF
dados_SIGA2$UF2[dados_SIGA2$UF=="N�O APLIC�VEL"]<-"ND"
dados_SIGA2$UF2[dados_SIGA2$UF=="N�O INFORMADO"]<-"ND"
dados_SIGA2$UF2[dados_SIGA2$UF=="CO"]<-"ND"
dados_SIGA2$UF2[dados_SIGA2$UF=="NE"]<-"ND"
dados_SIGA2$UF2[dados_SIGA2$UF=="EX"]<-"ND"
dados_SIGA2$UF2[dados_SIGA2$UF=="SD"]<-"ND"
dados_SIGA2$UF2[dados_SIGA2$UF=="SL"]<-"ND"
dados_SIGA2$UF2[dados_SIGA2$UF=="NO"]<-"ND"
dados_SIGA2$UF2[is.na(dados_SIGA2$UF)==T]<-"ND"

dados_SIGA<-dados_SIGA2
dados_SIOP<-dados_SIOP2



########################## AGREGANDO BASES DE DADOS POR ANO, UF E FUNCIONAL PARA CONFERENCIA

########################## SIOP
## EXTRAINDO PARTES MENORES
dados_SIOP$cod.fun<-substr(dados_SIOP$`Fun��o (desc#)`,1,2)
dados_SIOP$cod.subfun<-substr(dados_SIOP$`Subfun��o (desc#)`,1,3)
dados_SIOP$cod.pro<-substr(dados_SIOP$`Programa (desc#)`,1,4)  
dados_SIOP$cod.aca<-substr(dados_SIOP$`A��o (desc#)`,1,4)  
dados_SIOP$cod.loc<-substr(dados_SIOP$`Localizador (desc#)`,1,4)  

## DESENVOLVENDO O CODIGO FUNCIONAL NO SIOP
dados_SIOP$funcional<-paste(dados_SIOP$cod.fun,dados_SIOP$cod.subfun,dados_SIOP$cod.pro,dados_SIOP$cod.aca,dados_SIOP$cod.loc,sep=".")

## DESENVOLVENDO O CODIGO MODALIDADE NO SIOP
dados_SIOP$cod.nat<-"44"
dados_SIOP$cod.mod<-substr(dados_SIOP$`Modalidade (desc#)`,1,2)
dados_SIOP$cod.ele<-substr(dados_SIOP$`Elemento de Despesa (desc#)`,1,2)
dados_SIOP$cod.sub<-substr(dados_SIOP$Subelemento,7,8)

## DESEVOLVENDO O CODIGO DE MODALIDADE E ELEMENTO DO SIOP
dados_SIOP$modalidade<-paste(dados_SIOP$cod.nat,dados_SIOP$cod.mod,dados_SIOP$cod.ele,dados_SIOP$cod.sub,sep=".")    

## CRIANDO O CODIGO DE AGREGACAO
dados_SIOP$codagreg=paste(dados_SIOP[,2],dados_SIOP[,4],dados_SIOP$funcional,dados_SIOP$modalidade,sep="@")

## OBTENDO OS VALORES AGREGADOS
RAPEA<-tapply(dados_SIOP[,19],INDEX=list(dados_SIOP$codagreg), sum)
RAPIP<-tapply(dados_SIOP[,20],INDEX=list(dados_SIOP$codagreg), sum)
RAPINP<-tapply(dados_SIOP[,21],INDEX=list(dados_SIOP$codagreg), sum)
PRAP<-tapply(dados_SIOP[,22],INDEX=list(dados_SIOP$codagreg), sum)
A<-tapply(dados_SIOP[,24],INDEX=list(dados_SIOP$codagreg), sum)
E<-tapply(dados_SIOP[,25],INDEX=list(dados_SIOP$codagreg), sum)
EL<-tapply(dados_SIOP[,26],INDEX=list(dados_SIOP$codagreg), sum)
ELRAP<-tapply(dados_SIOP[,27],INDEX=list(dados_SIOP$codagreg), sum)
P<-tapply(dados_SIOP[,28],INDEX=list(dados_SIOP$codagreg), sum)



## DESENVOLVENDO O BANCO DE DADOS AGREGADO
agreg.SIOP=data.frame(RAPEA,RAPIP,RAPINP,PRAP,A,E,EL,ELRAP,P)   
cod.SIOP=row.names(agreg.SIOP)
agrega.SIOP=data.frame(RAPEA,RAPIP,RAPINP,PRAP,A,E,EL,ELRAP,P,cod.SIOP)   

########################## SIGA

## DESENVOLVENDO O CODIGO MODALIDADE NO SIGA
dados_SIGA$cod.nat<-"44"
dados_SIGA$cod.mod<-substr(dados_SIGA$Mod_Aplic,1,2)
dados_SIGA$cod.ele<-substr(dados_SIGA$Elemento_despesa,1,2)
dados_SIGA$cod.sub<-substr(dados_SIGA$Subelemento_despesa,7,8)

## DESENVOLVENDO O CODIGO FINAL DE MODALIDADE E ELEMENTO NO SIGA
dados_SIGA$modalidade<-paste(dados_SIGA$cod.nat,dados_SIGA$cod.mod,dados_SIGA$cod.ele,dados_SIGA$cod.sub,sep=".")

## CRIANDO O CODIGO DE AGREGACAO
dados_SIGA$codagreg=paste(dados_SIGA[,1],dados_SIGA$UF2,dados_SIGA$Funcional,dados_SIGA$modalidade,sep="@")

## AJUSTANDO VALORES PARA FORMATO NUMERICO
dados_SIGA[,12]<-gsub(pattern = ",",replacement = ".",x = dados_SIGA[,12])
dados_SIGA[,13]<-gsub(pattern = ",",replacement = ".",x = dados_SIGA[,13])
dados_SIGA[,14]<-gsub(pattern = ",",replacement = ".",x = dados_SIGA[,14])
dados_SIGA[,15]<-gsub(pattern = ",",replacement = ".",x = dados_SIGA[,15])
dados_SIGA[,16]<-gsub(pattern = ",",replacement = ".",x = dados_SIGA[,16])
dados_SIGA[,17]<-gsub(pattern = ",",replacement = ".",x = dados_SIGA[,17])

dados_SIGA[,12]=as.numeric(dados_SIGA[,12])
dados_SIGA[,13]=as.numeric(dados_SIGA[,13])
dados_SIGA[,14]=as.numeric(dados_SIGA[,14])
dados_SIGA[,15]=as.numeric(dados_SIGA[,15])
dados_SIGA[,16]=as.numeric(dados_SIGA[,16])
dados_SIGA[,17]=as.numeric(dados_SIGA[,17])

## OBTENDO OS VALORES AGREGADOS 
DI<-tapply(dados_SIGA[,12],INDEX=list(dados_SIGA$codagreg), sum)
A<-tapply(dados_SIGA[,13],INDEX=list(dados_SIGA$codagreg), sum)
E<-tapply(dados_SIGA[,14],INDEX=list(dados_SIGA$codagreg), sum)
L<-tapply(dados_SIGA[,15],INDEX=list(dados_SIGA$codagreg), sum)
P<-tapply(dados_SIGA[,16],INDEX=list(dados_SIGA$codagreg), sum)
RP<-tapply(dados_SIGA[,17],INDEX=list(dados_SIGA$codagreg), sum)

## DESENVOLVENDO O BANCO DE DADOS AGREGADO
agreg.SIGA=data.frame(DI,A,E,L,P,RP)   
cod.SIGA=row.names(agreg.SIGA)
cod.SIGA<-gsub("LI","NA",cod.SIGA)
agrega.SIGA=data.frame(DI,A,E,L,P,RP,cod.SIGA)   

## FAZENDO TESTE DE SOMAS
sum(dados_SIGA[,12])==sum(agrega.SIGA$DI)
sum(dados_SIGA[,13])==sum(agrega.SIGA$A)
sum(dados_SIGA[,14])==sum(agrega.SIGA$E)
sum(dados_SIGA[,15])==sum(agrega.SIGA$L)
sum(dados_SIGA[,16])==sum(agrega.SIGA$P)
sum(dados_SIGA[,17])==sum(agrega.SIGA$RP)

sum(dados_SIOP[,19])==sum(agrega.SIOP$RAPEA)
sum(dados_SIOP[,20])==sum(agrega.SIOP$RAPIP)
sum(dados_SIOP[,21])==sum(agrega.SIOP$RAPINP)
sum(dados_SIOP[,22])==sum(agrega.SIOP$PRAP)
sum(dados_SIOP[,24])==sum(agrega.SIOP$A)
sum(dados_SIOP[,25])==sum(agrega.SIOP$E)
sum(dados_SIOP[,26])==sum(agrega.SIOP$EL)
sum(dados_SIOP[,27])==sum(agrega.SIOP$ELRAP)
sum(dados_SIOP[,28])==sum(agrega.SIOP$P)

## SALVANDO OS ARQUIVOS
write.csv2(agrega.SIGA,file = "agrega_SIGA.csv", row.names = F)
write.csv2(agrega.SIOP,file = "agrega_SIOP.csv", row.names = F)



### TRABALHO NO EMPENHADO

sum(agrega.SIOP$E)
sum(agrega.SIGA$E)

library(data.table)
library(dplyr)


SIGA.teste<-data.frame(SIGA=agrega.SIGA$E,cod=agrega.SIGA$cod.SIGA)
SIOP.teste<-data.frame(SIOP=agrega.SIOP$E,cod=agrega.SIOP$cod.SIOP)

x=full_join(SIGA.teste,SIOP.teste)

### FAZENDO ALGUNS TESTES
x$SIOP[is.na(x$SIOP)==T]<-0
x$SIGA[is.na(x$SIGA)==T]<-0

x$teste<-x$SIGA==x$SIOP
x$diferenca<-x$SIGA-x$SIOP

table(x$teste)
sum(x$diferenca)
mean(x$diferenca)
summary(x$diferenca)
max(x$diferenca)

write.csv2(x,file = "Comparacao dos empenhados2.csv", row.names = F)




dados_SIGA$modalidade<-gsub(pattern = "LI","NA",dados_SIGA$modalidade)
dados_SIGA$modalidade<-gsub(pattern = "ES","NA",dados_SIGA$modalidade)

tab.siop<-table(dados_SIOP$modalidade)
tab.siga<-table(dados_SIGA$modalidade)

soma.siop<-aggregate(dados_SIOP$Empenhado_, by=list(dados_SIOP$modalidade), sum)
soma.siga<-aggregate(dados_SIGA$Empenhado, by=list(dados_SIGA$modalidade), sum)

siop.TAB<-cbind(tab.siop,soma.siop)
siga.TAB<-cbind(tab.siga,soma.siga)

write.csv2(x = siop.TAB,file = "siop.tabela.csv")
write.csv2(x = siga.TAB,file = "siga.tabela.csv")

xx<-rbind.data.frame(siop.TAB,siga.TAB)
x<-table(xx$Var1)
write.csv2(x = x,file = "tabelaapoio.csv")
