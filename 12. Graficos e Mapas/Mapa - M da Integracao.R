
# Abrindo pacotes

library(plyr)
library(dplyr)
library(ggmap)
library(ggplot2)
library(rgdal)
library(rgeos)
library(maptools)
library(tidyr)
library(tmap)

load(file = "siga - Base Final.RData")

siga<-dados.comp5
rm(dados.comp5)



#############################________________________TRABALHANDO OS DADOS
siga2<-siga[siga$`Orgão Superior*`=="53000 - MINISTERIO DA INTEGRACAO NACIONAL",]
tabela<-aggregate(siga2$`Executado IGP`, by=list(siga2$ANO,siga2$`UF*`), sum)

tabela2<-spread(tabela,Group.1,x)
tabela2<-rename(tabela2,UF=Group.2)

tabela3<-tabela2[-which(tabela2$UF%in%c("ND","EX","SD","NO","NE","SL","CO")),]


#############################________________________TRABALHANDO A MALHA

malha.brasil<-readShapeSpatial("12. Graficos e Mapas\\Shapes - Brasil por Estados\\estados_2010.shp")

malha.brasil@data<-rename(malha.brasil@data, UF=sigla)
malha.brasil@data<-inner_join(malha.brasil@data,tabela3)


ano<-2001:2016
eval(parse(text=paste0("g",ano,"<-tm_shape(malha.brasil) +
                         tm_fill('",ano,"', title = 'Investimento em MI', style = 'fixed',breaks = c(0, 100000000, 200000000, 300000000, 400000000, 500000000),
                                 textNA = 'Dunno') + tm_borders() +
                         tm_layout(legend.title.size = 1,legend.text.size = 0.6,legend.position = c('left','bottom'),legend.bg.color = 'white',
                                   legend.bg.alpha = 1)")))
library(gridExtra)
gridExtra::arrangeGrob(g2001,g2002,g2003,g2004,g2005,g2006,g2007,g2008,
                        g2009,g2010,g2011,g2012,g2013,g2014,g2015,g2016)



par(mfrow=c(4,4))

g2001
g2002
g2003
g2004
g2005
g2006
g2007
g2008
g2009
g2010
g2011
g2012
g2013
g2014
g2015
g2016

