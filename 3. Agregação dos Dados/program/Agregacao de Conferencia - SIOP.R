######### PROGRAMA��O PARA A AGREGACAO DE COMPARACAO ENTRE OS SIOPS

### NESSE CASO TEM-SE DUAS BASES DO SIOP, SENDO UMA MAIS AGREGADA QUE A OUTRA
### ASSIM, PARA A AGREGACAO SERA UTILIZADO SOMENTE AS INFORMACOES QUE ESTAO EM COMUM EM AMBAS AS BASES

## Abrindo base de trabalho
load(file.choose())
dados_SIOP<-SIOP_dados

## Abrindo base de conferencia
SIOP.conf<-read.csv2(file.choose(),h=T, stringsAsFactors = F, encoding = "ASCII")
str(SIOP.conf)

## Filtrando os anos a serem trabalhados (2001-2015)
conf_SIOP2<-subset(SIOP.conf, SIOP.conf$Ano>=2001 & SIOP.conf$Ano<=2015)

## Ajustando o formato das variaveis (char to num)
conf_SIOP2$Projeto.de.Lei<-gsub("\\.","",conf_SIOP2$Projeto.de.Lei)
conf_SIOP2$Dota��o.Inicial<-gsub("\\.","",conf_SIOP2$Dota��o.Inicial)
conf_SIOP2$Dota��o.Atual<-gsub("\\.","",conf_SIOP2$Dota��o.Atual)
conf_SIOP2$Empenhado<-gsub("\\.","",conf_SIOP2$Empenhado)
conf_SIOP2$Liquidado<-gsub("\\.","",conf_SIOP2$Liquidado)
conf_SIOP2$Pago<-gsub("\\.","",conf_SIOP2$Pago)

conf_SIOP2$Projeto.de.Lei<-as.numeric(conf_SIOP2$Projeto.de.Lei)
conf_SIOP2$Dota��o.Inicial<-as.numeric(conf_SIOP2$Dota��o.Inicial)
conf_SIOP2$Dota��o.Atual<-as.numeric(conf_SIOP2$Dota��o.Atual)
conf_SIOP2$Empenhado<-as.numeric(conf_SIOP2$Empenhado)
conf_SIOP2$Liquidado<-as.numeric(conf_SIOP2$Liquidado)
conf_SIOP2$Pago<-as.numeric(conf_SIOP2$Pago)

conf_SIOP<-conf_SIOP2

write.csv2(conf_SIOP,file ="SIOP_Comparacao.csv", row.names = F)


### AGREGACAO SIOP DADOS DE TRABALHO

## Desenvolver o codigo funcional parcial (programa, acao e localizador)
## EXTRAINDO PARTES MENORES
dados_SIOP$cod.pro<-substr(dados_SIOP$`Programa (desc#)`,1,4)  
dados_SIOP$cod.aca<-substr(dados_SIOP$`A��o (desc#)`,1,4)  
dados_SIOP$cod.loc<-substr(dados_SIOP$`Localizador (desc#)`,1,4)  

## DESENVOLVENDO O CODIGO FUNCIONAL NO SIOP
dados_SIOP$funcional<-paste(dados_SIOP$cod.pro,dados_SIOP$cod.aca,dados_SIOP$cod.loc,sep=".")

## Desenvolver o codigo de natureza completa parcial (gnd e modalidade)
dados_SIOP$cod.nat<-"44"
dados_SIOP$cod.mod<-substr(dados_SIOP$`Modalidade (desc#)`,1,2)

## DESEVOLVENDO O CODIGO DE MODALIDADE E ELEMENTO DO SIOP
dados_SIOP$modalidade<-paste(dados_SIOP$cod.nat,dados_SIOP$cod.mod,sep=".")  

## DESENVOLVENDO O COD DO ORGAO
dados_SIOP$cod.org<-substr(dados_SIOP$`�rg�o (desc#)`,1,5)

## Desenvolver codigo de agregacao (ano + orgao + funcional_parc + nat_comp_parc)
dados_SIOP$codagreg=paste(dados_SIOP$`Ano Exerc�cio`,dados_SIOP$cod.org,dados_SIOP$funcional,dados_SIOP$modalidade,sep="@")

## OBTENDO OS VALORES AGREGADOS
RAPEA<-tapply(dados_SIOP[,19],INDEX=list(dados_SIOP$codagreg), sum)
RAPIP<-tapply(dados_SIOP[,20],INDEX=list(dados_SIOP$codagreg), sum)
RAPINP<-tapply(dados_SIOP[,21],INDEX=list(dados_SIOP$codagreg), sum)
PRAP<-tapply(dados_SIOP[,22],INDEX=list(dados_SIOP$codagreg), sum)
A<-tapply(dados_SIOP[,24],INDEX=list(dados_SIOP$codagreg), sum)
E<-tapply(dados_SIOP[,25],INDEX=list(dados_SIOP$codagreg), sum)
EL<-tapply(dados_SIOP[,26],INDEX=list(dados_SIOP$codagreg), sum)
ELRAP<-tapply(dados_SIOP[,27],INDEX=list(dados_SIOP$codagreg), sum)
P<-tapply(dados_SIOP[,28],INDEX=list(dados_SIOP$codagreg), sum)

## DESENVOLVENDO O BANCO DE DADOS AGREGADO
agreg.SIOP=data.frame(RAPEA,RAPIP,RAPINP,PRAP,A,E,EL,ELRAP,P)   
cod.SIOP=row.names(agreg.SIOP)
agrega.SIOP=data.frame(RAPEA,RAPIP,RAPINP,PRAP,A,E,EL,ELRAP,P,cod.SIOP)   



### AGREGACAO SIOP DADOS DE CONFERENCIA

## Desenvolver o codigo funcional parcial (programa, acao e localizador)
## EXTRAINDO PARTES MENORES
conf_SIOP$cod.pro<-substr(conf_SIOP$Programa,1,4)  
conf_SIOP$cod.aca<-substr(conf_SIOP$A��o,1,4)  
conf_SIOP$cod.loc<-substr(conf_SIOP$Localizador,1,4)  

## DESENVOLVENDO O CODIGO FUNCIONAL NO SIOP
conf_SIOP$funcional<-paste(conf_SIOP$cod.pro,conf_SIOP$cod.aca,conf_SIOP$cod.loc,sep=".")

## Desenvolver o codigo de natureza completa parcial (gnd e modalidade)
conf_SIOP$cod.nat<-"44"
conf_SIOP$cod.mod<-substr(conf_SIOP$Modalidade.de.Aplica��o,1,2)

## DESEVOLVENDO O CODIGO DE MODALIDADE E ELEMENTO DO SIOP
conf_SIOP$modalidade<-paste(conf_SIOP$cod.nat,conf_SIOP$cod.mod,sep=".")    

## DESENVOLVENDO O COD DO ORGAO
conf_SIOP$cod.org<-substr(conf_SIOP$�rg�o.Or�ament�rio,1,5)

## Desenvolver codigo de agregacao (ano + orgao + funcional_parc + nat_comp_parc)
conf_SIOP$codagreg=paste(conf_SIOP$Ano,conf_SIOP$cod.org,conf_SIOP$funcional,conf_SIOP$modalidade,sep="@")

## OBTENDO OS VALORES AGREGADOS
PL<-tapply(conf_SIOP$Projeto.de.Lei,INDEX=list(conf_SIOP$codagreg), sum)
DI<-tapply(conf_SIOP$Dota��o.Inicial,INDEX=list(conf_SIOP$codagreg), sum)
DA<-tapply(conf_SIOP$Dota��o.Atual,INDEX=list(conf_SIOP$codagreg), sum)
E<-tapply(conf_SIOP$Empenhado,INDEX=list(conf_SIOP$codagreg), sum)
L<-tapply(conf_SIOP$Liquidado,INDEX=list(conf_SIOP$codagreg), sum)
P<-tapply(conf_SIOP$Pago,INDEX=list(conf_SIOP$codagreg), sum)

## DESENVOLVENDO O BANCO DE DADOS AGREGADO
agreg.SIOP2=data.frame(PL,DI,DA,E,L,P)   
cod.SIOP2=row.names(agreg.SIOP2)
agrega.SIOP2=data.frame(PL,DI,DA,E,L,P,cod.SIOP2)   





############# Teste de comparacao
sum(agrega.SIOP$E)
sum(agrega.SIOP2$E)

library(data.table)
library(dplyr)

conf.teste<-data.frame(conf=agrega.SIOP2$E,cod=agrega.SIOP2$cod.SIOP2)
SIOP.teste<-data.frame(SIOP=agrega.SIOP$E,cod=agrega.SIOP$cod.SIOP)

x=full_join(conf.teste,SIOP.teste)

### FAZENDO ALGUNS TESTES
x$SIOP[is.na(x$SIOP)==T]<-0
x$conf[is.na(x$conf)==T]<-0

x$teste<-x$conf==x$SIOP
x$diferenca<-x$conf-x$SIOP

table(x$teste)
sum(x$diferenca)
mean(x$diferenca)
summary(x$diferenca)
max(x$diferenca)

write.csv2(x,file = "Comparacao dos empenhados.csv", row.names = F)


