##### Padronizando nomenclaturas


#########################################_______________________REGIAO

dados2$Região0<-dados2$Região

dados2$Região0[dados2$Região=="REGIÃO SUL"]<-"SL"
dados2$Região0[dados2$Região=="REGIÃO SUDESTE"]<-"SD"
dados2$Região0[dados2$Região=="REGIÃO NORTE"]<-"NO"
dados2$Região0[dados2$Região=="REGIÃO NORDESTE"]<-"NE"
dados2$Região0[dados2$Região=="REGIÃO CENTRO OESTE"]<-"CO"
dados2$Região0[dados2$Região=="EXTERIOR"]<-"EX"

dados2$Região0[dados2$Região=="NA"]<-"ND"
dados2$Região0[dados2$Região=="NACIONAL"]<-"ND"
dados2$Região0[dados2$Região=="NÃO APLICÁVEL"]<-"ND"
dados2$Região0[dados2$Região=="NÃO INFORMADO"]<-"ND"

dados2$Região6[dados2$UF6=="AC"|dados2$UF6=="AM"|dados2$UF6=="RR"|
                      dados2$UF6=="RO"|dados2$UF6=="PA"|dados2$UF6=="AP"|dados2$UF6=="TO"]<-"NO"

dados2$Região6[dados2$UF6=="MA"|dados2$UF6=="RN"|dados2$UF6=="PB"|dados2$UF6=="SE"|dados2$UF6=="BA"|
                      dados2$UF6=="PI"|dados2$UF6=="CE"|dados2$UF6=="PE"|dados2$UF6=="AL"]<-"NE"

dados2$Região6[dados2$UF6=="MT"|dados2$UF6=="MS"|dados2$UF6=="GO"|dados2$UF6=="DF"]<-"CO"

dados2$Região6[dados2$UF6=="MG"|dados2$UF6=="ES"|dados2$UF6=="SP"|dados2$UF6=="RJ"]<-"SD"

dados2$Região6[dados2$UF6=="RS"|dados2$UF6=="SC"|dados2$UF6=="PR"]<-"SL"
dados2$Região6[dados2$UF6=="EX"]<-"EX"

table(dados2$Região)
table(dados2$Região0)
table(dados2$Região6)

#########################################_______________________ORGAO
names(table(dados2$`Órgão Superior (Cod/Desc)`))
dados2$Orgao_Superior<-dados2$`Órgão Superior (Cod/Desc)`
dados2$Orgao_Superior[dados2$`Órgão Superior (Cod/Desc)`=="13000 - JUSTICA MILITAR DA UNIAO"]<-"13000 - JUSTICA MILITAR" 
dados2$Orgao_Superior[dados2$`Órgão Superior (Cod/Desc)`=="20113 - MINISTERIO DO PLANEJAMENTO,ORCAMENTO E GESTAO"]<-"20113 - MINIST. DO PLANEJAMENTO, DESENVOLV. E GESTAO"
dados2$Orgao_Superior[dados2$`Órgão Superior (Cod/Desc)`=="22000 - MINIST, DA AGRICUL,,PECUARIA E ABASTECIMENTO"]<-"22000 - MINIST. DA AGRICUL.,PECUARIA E ABASTECIMENTO" 
dados2$Orgao_Superior[dados2$`Órgão Superior (Cod/Desc)`=="22000 - MINISTERIO DA AGRICULTURA, PECUARIA E ABASTECIMENTO"]<-"22000 - MINIST. DA AGRICUL.,PECUARIA E ABASTECIMENTO" 
dados2$Orgao_Superior[dados2$`Órgão Superior (Cod/Desc)`=="24000 - MINISTERIO DA CIENCIA E TECNOLOGIA"]<-"24000 - MINIST.DA CIENCIA,TECNOL.,INOV.E COMUNICACOES"
dados2$Orgao_Superior[dados2$`Órgão Superior (Cod/Desc)`=="24000 - MINISTERIO DA CIENCIA, TECNOLOGIA E INOVACAO"]<-"24000 - MINIST.DA CIENCIA,TECNOL.,INOV.E COMUNICACOES"
dados2$Orgao_Superior[dados2$`Órgão Superior (Cod/Desc)`=="28000 - MINISTERIO DO DESENV,IND, E COMERCIO EXTERIOR"]<-"28000 - MINIST. DA INDUSTRIA, COM.EXTERIOR E SERVICOS"
dados2$Orgao_Superior[dados2$`Órgão Superior (Cod/Desc)`=="28000 - MINISTERIO DO DESENV,IND. E COMERCIO EXTERIOR"]<-"28000 - MINIST. DA INDUSTRIA, COM.EXTERIOR E SERVICOS"
dados2$Orgao_Superior[dados2$`Órgão Superior (Cod/Desc)`=="28000 - MINISTERIO DO DESENVOLVIMENTO, IND,E COMERCIO"]<-"28000 - MINIST. DA INDUSTRIA, COM.EXTERIOR E SERVICOS"
dados2$Orgao_Superior[dados2$`Órgão Superior (Cod/Desc)`=="28000 - MINISTERIO DO DESENVOLVIMENTO, INDUSTRIA E COMERCIO EXTERIOR"]<-"28000 - MINIST. DA INDUSTRIA, COM.EXTERIOR E SERVICOS"
dados2$Orgao_Superior[dados2$`Órgão Superior (Cod/Desc)`=="30000 - MINISTERIO DA JUSTICA"]<-"30000 - MINISTERIO DA JUSTICA E CIDADANIA"
dados2$Orgao_Superior[dados2$`Órgão Superior (Cod/Desc)`=="33000 - MINISTERIO DA PREVIDENCIA E ASSIST, SOCIAL"]<-"33000 - MINISTERIO DA PREVIDENCIA SOCIAL"     
dados2$Orgao_Superior[dados2$`Órgão Superior (Cod/Desc)`=="39000 - MINISTERIO DOS TRANSPORTES"]<-"39000 - MINIST.DOS TRANSP.,PORTOS E AVIACAO CIVIL"    
dados2$Orgao_Superior[dados2$`Órgão Superior (Cod/Desc)`=="51000 - MINISTERIO DO ESPORTE E TURISMO"]<-"51000 - MINISTERIO DO ESPORTE"          
dados2$Orgao_Superior[dados2$`Órgão Superior (Cod/Desc)`=="55000 - MINISTERIO DA ASSISTENCIA  SOCIAL"]<-"55000 - MINISTERIO DO DESENVOLVIM. SOCIAL E AGRARIO"  
dados2$Orgao_Superior[dados2$`Órgão Superior (Cod/Desc)`=="55000 - MINISTERIO DO DESENV, SOCIAL E COMBATE A FOME"]<-"55000 - MINISTERIO DO DESENVOLVIM. SOCIAL E AGRARIO"  
dados2$Orgao_Superior[dados2$`Órgão Superior (Cod/Desc)`=="55000 - MINISTERIO DO DESENV. SOCIAL E COMBATE A FOME"]<-"55000 - MINISTERIO DO DESENVOLVIM. SOCIAL E AGRARIO"  
dados2$Orgao_Superior[dados2$`Órgão Superior (Cod/Desc)`=="55000 - MINISTERIO DO DESENVOLVIMENTO SOCIAL E COMBATE À FOME"]<-"55000 - MINISTERIO DO DESENVOLVIM. SOCIAL E AGRARIO"  
dados2$Orgao_Superior[dados2$`Órgão Superior (Cod/Desc)`=="58000 - MINISTERIO DA PESCA E AQÜICULTURA"]<-"58000 - MINISTERIO DA PESCA E AQUICULTURA"            

names(table(dados2$Orgao_Superior))



#dados2$Acao2<-iconv(dados2$`Ação (Cod/Desc)`, to = "ASCII//TRANSLIT")
dados3<-aggregate(dados2[,c(31,82)],by=dados2[,c(1,2,3,6,12,13,14,15,16,20,21,42,112,113,118)], sum)

dados3<-dados3[dados3$`Despesa Executada`>0,]

write.csv2(dados3,"DADOS_RESUMO.csv")
write.csv2(dados2,"Siga_uf6.csv")






dados.teste



