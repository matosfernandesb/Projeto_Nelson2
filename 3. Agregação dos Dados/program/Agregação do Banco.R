##### Agrega��o do Banco de Dados

library(readxl)


attach(SIOP_dados)
str(SIOP_dados)
SIOP_dados$cod3=paste(SIOP_dados[,2],SIOP_dados[,3],SIOP_dados[,4],SIOP_dados[,5],SIOP_dados[,6], SIOP_dados[,7],SIOP_dados[,8],
                      SIOP_dados[,9],SIOP_dados[,10],SIOP_dados[,11],SIOP_dados[,12],SIOP_dados[,13],
                      SIOP_dados[,14],SIOP_dados[,15],SIOP_dados[,16],sep="@")

RAPEA<-tapply(SIOP_dados[,19],INDEX=list(SIOP_dados$cod3), sum)
RAPIP<-tapply(SIOP_dados[,20],INDEX=list(SIOP_dados$cod3), sum)
RAPINP<-tapply(SIOP_dados[,21],INDEX=list(SIOP_dados$cod3), sum)
PRAP<-tapply(SIOP_dados[,22],INDEX=list(SIOP_dados$cod3), sum)
A<-tapply(SIOP_dados[,24],INDEX=list(SIOP_dados$cod3), sum)
E<-tapply(SIOP_dados[,25],INDEX=list(SIOP_dados$cod3), sum)
EL<-tapply(SIOP_dados[,26],INDEX=list(SIOP_dados$cod3), sum)
ELRAP<-tapply(SIOP_dados[,27],INDEX=list(SIOP_dados$cod3), sum)
P<-tapply(SIOP_dados[,28],INDEX=list(SIOP_dados$cod3), sum)

agreg=data.frame(RAPEA,RAPIP,RAPINP,PRAP,A,E,EL,ELRAP,P)   

cod3=row.names(agreg)
agrega=data.frame(RAPEA,RAPIP,RAPINP,PRAP,A,E,EL,ELRAP,P,cod3)   

write.csv2(agrega,"SIOP_agregado.csv")          
save(agrega,file="SIOP_agregado.RData")          

### Conferindo os resultados
tapply(SIOP_dados$Autorizado_,INDEX=SIOP_dados$UF,sum)

agrega$teste=agrega$E==agrega$EL
agrega2=agrega[agrega$teste==F,]

agrega2$dif<-agrega2$E-agrega2$EL









table(SIOP_dados$Subelemento)

SIOP_dados$codSE<-substr(SIOP_dados$Subelemento, 7, 8)


uf_cod=read_excel(file.choose())

SIOP_dados2=merge(SIOP_dados,uf_cod)


for(i in 1:length(SIOP_dados$cod)){
  if(is.na(SIOP_dados$codSE[i])){SIOP_dados$codSE[i]<-"50"}
}


for(i in 1:length(SIOP_dados$cod)){
      if(SIOP_dados$codSE[i]!="01" |SIOP_dados$codSE[i]!="03" |SIOP_dados$codSE[i]!="04" |SIOP_dados$codSE[i]!="05" |SIOP_dados$codSE[i]!="07" |
         SIOP_dados$codSE[i]!="09" |SIOP_dados$codSE[i]!="11" |SIOP_dados$codSE[i]!="13" |SIOP_dados$codSE[i]!="15" |SIOP_dados$codSE[i]!="17" |
         SIOP_dados$codSE[i]!="19" |SIOP_dados$codSE[i]!="21" |SIOP_dados$codSE[i]!="23" |SIOP_dados$codSE[i]!="25" |SIOP_dados$codSE[i]!="27" |
         SIOP_dados$codSE[i]!="29" |SIOP_dados$codSE[i]!="31" |SIOP_dados$codSE[i]!="33" |SIOP_dados$codSE[i]!="35" |SIOP_dados$codSE[i]!="37" |
         SIOP_dados$codSE[i]!="39" |SIOP_dados$codSE[i]!="41" |SIOP_dados$codSE[i]!="42" |SIOP_dados$codSE[i]!="43" |SIOP_dados$codSE[i]!="45" |
         SIOP_dados$codSE[i]!="47" |SIOP_dados$codSE[i]!="49" ){
        SIOP_dados$codSE[i]<-"50"
      }
}
